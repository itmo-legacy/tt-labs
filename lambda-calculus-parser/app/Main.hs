module Main where

import           Data.Either   (either)
import           Data.Function ((&))

import           Lib
import qualified LTerm         as LT
import           Utils         ((|>))

main :: IO ()
main = do
  parsedLine <- getContents |> parseLTerm
  parsedLine & either
                 (\msg -> putStrLn $ "Oops! " ++ msg)
                 (\parsed -> putStrLn $ LT.toString parsed)

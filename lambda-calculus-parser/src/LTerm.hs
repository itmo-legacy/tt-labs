module LTerm (LTerm(..), toString) where

import           Text.Printf (printf)

data LTerm = L String LTerm
           | A LTerm LTerm
           | V String
           deriving (Show, Eq)

toString :: LTerm -> String
toString (L v t)   = printf "(\\%s.%s)" v (toString t)
toString (A t1 t2) = printf "(%s %s)" (toString t1) (toString t2)
toString (V s)     = s

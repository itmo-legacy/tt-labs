{-# LANGUAGE LambdaCase #-}

module Lib (parseLTerm) where

import qualified Data.ByteString.Char8 as BS (pack)
import           Data.Function         ((&))
import           Text.Printf           (printf)

import           Lexer                 (tokenize)
import           LTerm                 (LTerm (..), toString)
import           LTermParser
import           Utils                 ((|>))


parseLTerm :: String -> Either String LTerm
parseLTerm s = s & BS.pack & tokenize
          |> runP ltermP
          >>= \case Just (term, []) -> Right term
                    Just (term, ts) -> Left $ printf "'%s' is left unparsed (parsed: '%s')" (show ts) (show term)
                    Nothing -> Left "parsing failed"

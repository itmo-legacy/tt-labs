{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms   #-}
{-# LANGUAGE ViewPatterns      #-}

module Lexer (Token(..), tokenize) where

import qualified Data.ByteString.Char8 as BS
import           Data.Char             (isSpace)
import           Data.Function         ((&))

import           Stream                (pattern (:<), pattern Nil)
import           Utils                 ((|>))

data Token
    = OpParenT
    | ClParenT
    | DotT
    | SlashT
    | IdT String
    deriving (Eq, Show)

tokenize :: BS.ByteString -> Either String [Token]
tokenize Nil = return []
tokenize ('(':<cs) = (OpParenT :) <$> tokenize cs
tokenize (')':<cs) = (ClParenT :) <$> tokenize cs
tokenize ('.':<cs) = (DotT :) <$> tokenize cs
tokenize ('\\':<cs) = (SlashT :) <$> tokenize cs
tokenize (lexIdent -> Just (ident, rest)) = (ident :) <$> tokenize rest
tokenize (c:<cs) | isSpace c = cs & BS.span isSpace & snd & tokenize
tokenize (c:<cs) = Left ("Unexpected symbol: '" ++ [c] ++ "', followed by '" ++ (BS.unpack cs) ++ "'")

lexIdent :: BS.ByteString -> Maybe (Token, BS.ByteString)
lexIdent (c:<cs) | firstInIdent c = return (IdT $ BS.unpack (c:<restOfIdent), restOfCs)
                 where firstInIdent = (`elem` ['a'..'z'])
                       nextInIdent = (`elem` ['a'..'z'] ++ ['0'..'9'] ++ ['\''])
                       (restOfIdent, restOfCs) = BS.span nextInIdent cs
lexIdent _ = Nothing

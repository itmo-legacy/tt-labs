module LTermParserSpec (spec) where

import           Test.Hspec

import           Lib         (parseLTerm)
import           LTerm       (toString)
import           LTermParser

parse :: String -> Either String LTerm
parse = parseLTerm

parenthesize :: String -> Either String String
parenthesize s = toString <$> parse s

spec :: Spec
spec = do
  describe "LTermParser" $ do
    it "parses the first test case correctly" $ do
      parenthesize "\\a.\\b.a b c (\\d.e \\f.g) h" `shouldBe` return "(\\a.(\\b.((((a b) c) (\\d.(e (\\f.g)))) h)))"

    it "parses the second test case properly" $ do
      parenthesize "((a\\bbb.c)d)e\nf g" `shouldBe` return "(((((a (\\bbb.c)) d) e) f) g)"

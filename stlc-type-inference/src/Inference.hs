{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE OverloadedStrings #-}

module Inference (Inference (..), Rule (..), inferType) where

import           Control.Applicative     ((<|>))
import           Control.Monad           (foldM)
import           Data.ByteString.Builder (intDec, string7)
import qualified Data.ByteString.Builder as BS (Builder)
import           Data.Function           ((&))
import qualified Data.HashMap.Strict     as M
import qualified Data.HashSet            as S
import           Data.List               (intersperse)
import           Data.Monoid             ((<>))

import           LTerm                   (LTerm (..), LType (..))
import           Utils                   (ToBSBuilder (..), (|>))

data Inference = Inference { infCtxt :: Context
                           , infTerm :: LTerm
                           , infType :: LType
                           , infRule :: Rule
                           } deriving Show

type Context = M.HashMap String LType
data Rule = Rule1 | Rule2 Inference Inference | Rule3 Inference deriving Show


instance ToBSBuilder Inference where
  toBSBuilder = toBSBuilderHelper ""

toBSBuilderHelper :: BS.Builder -> Inference -> BS.Builder
toBSBuilderHelper prefix (Inference ctxt lterm ltype rule)
  = mconcat [prefix, ctxtB, "|- ", ltermB, " : ", ltypeB, " [rule #", intDec (ruleNumber rule), "]", proofB]
  where
    preCtxtB :: BS.Builder
    preCtxtB = ctxt & M.toList & map (\(k, v) -> string7 k <> " : " <> toBSBuilder v) & intersperse ", " & mconcat
    ctxtB = if null ctxt then "" else preCtxtB <> " "
    ltermB = toBSBuilder lterm
    ltypeB = toBSBuilder ltype
    proofB = case rule of
      Rule1 -> ""
      Rule2 inf1 inf2 -> "\n" <> toBSBuilderHelper newPrefix inf1 <> "\n" <> toBSBuilderHelper newPrefix inf2
      Rule3 inf -> "\n" <> toBSBuilderHelper newPrefix inf
    newPrefix = "*   " <> prefix

ruleNumber :: Rule -> Int
ruleNumber Rule1       = 1
ruleNumber (Rule2 _ _) = 2
ruleNumber (Rule3 _)   = 3

inferType :: LTerm -> Maybe Inference
inferType lterm = do
  let (!system, !vars) = makeSystem lterm
  !preSubst <- solveSystem system
  let subst = fmap (\v -> M.lookupDefault (T v) v preSubst) vars
  let !ctxt = M.fromList [(v, substVar subst v) | v <- freeVariables lterm]
  !proof <- proveType subst ctxt lterm
  return proof

proveType :: Substitution -> Context -> LTerm -> Maybe Inference
proveType _ ctxt lterm@(V v) = M.lookup v ctxt |> \ltype -> Inference ctxt lterm ltype Rule1
proveType subst ctxt lterm@(A l1 l2) = do !inf1 <- proveType subst ctxt l1
                                          !inf2 <- proveType subst ctxt l2
                                          (_, resType) <- unroll (infType inf1)
                                          return (Inference ctxt lterm resType (Rule2 inf1 inf2))
  where unroll (t1 :-> t2) = Just (t1, t2)
        unroll _           = Nothing
proveType subst ctxt lterm@(L x l) = do let tx = substVar subst x
                                        !inf <- proveType subst (M.insert x tx ctxt) l
                                        let !resType = tx :-> infType inf
                                        return (Inference ctxt lterm resType (Rule3 inf))

freeVariables :: LTerm -> [String]
freeVariables lterm = go S.empty [] lterm & snd
  where go bound free (V v) | v `elem` bound = (bound, free)
                            | otherwise      = (bound, v:free)
        go bound free (A l1 l2) = go bound' free' l2
          where (!bound', !free') = go bound free l1
        go bound free (L x l) = go ((S.insert $! x) $! bound) free l


type Equation = (LType, LType)
type System = [Equation]

makeSystem :: LTerm -> (System, M.HashMap String String)
makeSystem lterm = go (1 :: Int) M.empty lterm & \(s, _, _, vars) -> (s, vars)
  where go typeNo vars (V v) = ([], T vtype, typeNo, M.insert v vtype vars)
          where vtype = M.lookupDefault v v vars
        go typeNo vars (A f x) = ([(tx :-> ta, tf)] ++ sf ++ sx, ta, typeNo'' + 1, vars'')
          where (!sf, !tf, !typeNo', !vars') = go typeNo vars f
                (!sx, !tx, !typeNo'', !vars'') = go typeNo' vars' x
                !ta = T $ show typeNo''
        go typeNo vars (L v b) = (sb, T freshV :-> tb, typeNo', vars')
          where (!sb, !tb, !typeNo', !vars') = go (typeNo + 1) (M.insert v freshV vars) b
                freshV = show typeNo


solveSystem :: System -> Maybe Substitution
solveSystem system
  | isInconsistent = Nothing
  | otherwise = extractSolution <|> solveSystem simplified
  where isInconsistent = not $! all isEquationConsistent system
        extractSolution = foldM checkEquation (M.empty, S.empty) system |> fst
        simplified = simplify [] system

        isEquationConsistent (T t1, T t2) | t1 == t2 = True
        isEquationConsistent (T t1, rhs)  | t1 `elem` getVariables rhs = False
        isEquationConsistent _            = True

        checkEquation (subst, vars) (T a, rhs)
          | not (a `M.member` subst)
            && not (a `elem` vars)
            && (null $ S.intersection (M.keysSet subst) rhsVars)
            = Just (M.insert a rhs subst, S.union vars rhsVars)
          where rhsVars = getVariables rhs & S.fromList
        checkEquation _ _ = Nothing

        simplify acc [] = acc
        simplify acc ((l1 :-> l2, r1 :-> r2):xs) = simplify ((l1, r1):(l2, r2):acc) xs
        simplify acc ((T a, T a'):xs) | a == a' = simplify acc xs
        simplify acc ((lhs, T a):xs) = simplifyDef acc a lhs xs
        simplify acc ((T a, rhs):xs) = simplifyDef acc a rhs xs

        simplifyDef acc a other xs = simplify ((T a, other):acc') xs'
          where substA = substSystem $! (M.singleton a other)
                acc'   = substA acc
                xs'    = substA xs

        getVariables (T t)       = [t]
        getVariables (t1 :-> t2) = getVariables t1 ++ getVariables t2


type Substitution = M.HashMap String LType

substVar :: Substitution -> String -> LType
substVar s v = M.lookupDefault (T v) v s

substType :: Substitution -> LType -> LType
substType s (t1 :-> t2) = (substType s t1) :-> (substType s t2)
substType s (T t)       = M.lookupDefault (T t) t s

substEq :: Substitution -> Equation -> Equation
substEq s (lhs, rhs) = (substType s lhs, substType s rhs)

substSystem :: Substitution -> System -> System
substSystem = map . substEq


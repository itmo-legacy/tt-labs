{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Arrow           (first, (>>>))
import           Data.ByteString.Builder (hPutBuilder)
import           Data.Function           ((&))
import qualified Data.HashMap.Strict     as M
import           Data.Monoid             ((<>))
import           System.IO               (stdout)

import           Inference               (Inference (..), Rule (..))
import           Lib                     (inferType, parseLTerm, toBSBuilder)
import           LTerm                   (LTerm (..))

main :: IO ()
main = do
  eitherLterm <- parseLTerm <$> getLine
  eitherLterm & either
    (putStrLn . ("ERR: " ++))
    (rename >>> inferType >>> maybe
      (putStrLn "Expression has no type")
      (hPutBuilder stdout . (<> "\n") . toBSBuilder . renameBackInf))

rename :: LTerm -> LTerm
rename = go M.empty >>> snd
  where go ns (V v)     = (ns, V $ maybe v (combineWithNum v) (M.lookup v ns))
        go ns (A l1 l2) = let (ns1, l1') = go ns l1
                              (ns2, l2') = go ns1 l2
                          in  (ns2, A l1' l2')
        go ns (L v l)   = let next_n = 1 + M.lookupDefault 0 v ns
                              (ns', l') = go (M.insert v next_n ns) l
                          in (ns', (L (combineWithNum v next_n) l'))

        combineWithNum :: String -> Int -> String
        combineWithNum v n = v ++ "_" ++ show n

renameBack :: LTerm -> LTerm
renameBack (V v)     = V (demangle v)
renameBack (A l1 l2) = A (renameBack l1) (renameBack l2)
renameBack (L v l)   = L (demangle v) (renameBack l)

demangle :: String -> String
demangle = takeWhile (/= '_')

renameBackInf :: Inference -> Inference
renameBackInf (Inference ctxt lterm ltype rule) = Inference ctxt' lterm' ltype rule'
  where ctxt' = ctxt & M.toList & map (first demangle) & M.fromList
        lterm' = renameBack lterm
        rule' = mapRule renameBackInf rule

mapRule :: (Inference -> Inference) -> Rule -> Rule
mapRule _ Rule1         = Rule1
mapRule f (Rule2 i1 i2) = Rule2 (f i1) (f i2)
mapRule f (Rule3 i)     = Rule3 (f i)
